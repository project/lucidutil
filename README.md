Lucid Module / help Functions
================================


Features:
---------

1. Use Image Styles in templates / modules

`image_style(node.field_image.entity.fileuri, 'image_style_name')`

2. Special twig functions (subset of twig tweak module)

3. Custom theme suggestions:  
   40X pages template eg.: page--404.html.twig
  
4. ckEditor 'detail' plugin added

5. Redirect user page to /dashborad

6. Empty field formatter, if user dont fill field still display the non-filled field
