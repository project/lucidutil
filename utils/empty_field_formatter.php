<?php

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FormatterInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Implements hook_field_formatter_third_party_settings_form().
 */
function lucidutil_field_formatter_third_party_settings_form(FormatterInterface $plugin, FieldDefinitionInterface $field_definition, $view_mode, $form, FormStateInterface $form_state) {
  // @todo Implement plugin manager.
  $element['empty_field_value'] = [
    '#type' => 'select',
    '#title' => t('Display options for empty fields'),
    '#default_value' => $plugin->getThirdPartySetting('lucidutil', 'empty_field_value', ''),
    '#options' => [
      '' => t('When empty: exclude from display'),
      'empty_string' => t('When empty: show anyway'),
      'empty_message' => t('When empty: show notification'),
    ],
  ];
  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary_alter().
 */
function lucidutil_field_formatter_settings_summary_alter(&$summary, $context) {
  $handler = $context['formatter']->getThirdPartySetting('lucidutil', 'empty_field_value', '');
  if ($handler === 'empty_string') {
    $summary[] = t('When empty: show anyway');
  }
  elseif ($handler === 'empty_message') {
    $summary[] = t('When empty: show anyway and pisplay N/A as value');
  }
}

/**
 * Implements hook_entity_display_build_alter().
 *
 * Showing empty fields, by default it will be hidden.
 */
function lucidutil_entity_display_build_alter(&$build, $context) {
  /** @var \Drupal\Core\Entity\EntityInterface $entity */
  $entity = $context['entity'];
  /** @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display */
  $display = $context['display'];
  foreach (Element::children($build) as $field_name) {
    if (
      $entity->get($field_name)->isEmpty()
      && ($handler = $display->getRenderer($field_name)->getThirdPartySetting('lucidutil', 'empty_field_value'))
    ) {
      /** @var \Drupal\Core\Field\FieldDefinitionInterface $definition */
      $definition = $entity->get($field_name)->getFieldDefinition();
      $component = $display->getComponent($field_name);
      $build[$field_name] =
        [
          '#theme' => 'field',
          '#title' => $definition->getLabel(),
          '#label_display' => $component['label'],
          '#view_mode' => $context['view_mode'],
          '#language' => $entity->get($field_name)->getLangcode(),
          '#field_name' => $definition->getName(),
          '#field_type' => 'string',
          '#field_translatable' => $definition->isTranslatable(),
          '#entity_type' => $entity->getEntityTypeId(),
          '#bundle' => $entity->bundle(),
          '#object' => $entity,
          '#items' => [(object) ['_attributes' => []]],
          '#is_multiple' => FALSE,
          '#formatter' => 'string',
        ] + $build[$field_name];
      if ($handler == 'empty_message') {
        $build[$field_name][] = [
          '#type' => 'container',
          '#attributes' => [
            'class' => ['messages messages--warning'],
          ],
          'empty_message' => ['#plain_text' => t('N/A')],
        ];
      }
    }
  }
}
