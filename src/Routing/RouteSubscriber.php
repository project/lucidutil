<?php

namespace Drupal\lucidutil\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Change the route associated with the user profile page (/user, /user/{uid}).
    if ($route = $collection->get('user.page')) {
      $route->setDefault('_controller', '\Drupal\lucidutil\Controller\DashboardRedirectController::userPage');
    }
    if ($route = $collection->get('entity.user.canonical')) {
      $route->setDefault('_controller', '\Drupal\lucidutil\Controller\DashboardRedirectController::userPage');
    }
  }

}
