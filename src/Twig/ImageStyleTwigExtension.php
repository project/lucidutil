<?php

namespace Drupal\lucidutil\Twig;

use Drupal\image\Entity\ImageStyle;

/**
 * Class ImageStyleTwigExtension.
 */
class ImageStyleTwigExtension extends \Twig_Extension {

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'image_style';
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction(
      // Fct. name.
        'image_style',
        // Fct. callable.
        [$this, 'imageStyleUrl'],
        // Options.
        [
          'is_safe' => ['html'],
        ]
      ),
    ];
  }

  /**
   * Get the URL to a styled image version.
   */
  public function imageStyleUrl($uri, $image_style_id) {
    $image_style_url = '';

    if ($image_style = ImageStyle::load($image_style_id)) {
      $image_style_url = $image_style->buildUrl($uri);
    }
    else {
      \Drupal::logger('lucidutil')->warning("ImageStyleTwigExtension: image style '$image_style_id' not defined.");
    }

    return $image_style_url;
  }

}
