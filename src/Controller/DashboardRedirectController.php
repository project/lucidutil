<?php

namespace Drupal\lucidutil\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class DashboardRedirectController.
 */
class DashboardRedirectController extends ControllerBase {

  /**
   * Dashboard Render.
   *
   * @return array
   *   Return username & user_edit_form_url.
   */
  public function dashboardRender() {
    $user = \Drupal::currentUser();
    $user_id = $user->id();
    $username = $user->getAccountName();
    $user_edit_form_url = \Drupal\Core\Url::fromRoute('entity.user.edit_form', ['user' => $user_id])->toString();

    return [
      '#theme' => 'dashboard',
      '#title' => 'My Dashboard',
      '#username' => $username,
      '#user_edit_form_url' => $user_edit_form_url,
    ];
  }

  /**
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function userPage() {
    // Set the redirect destination.
    return $this->redirect('lucidutil.dashboard_redirect_controller_dashboard_redirect');
  }

}
